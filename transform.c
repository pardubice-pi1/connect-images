#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

struct poz_t{
    int x;
    int y;
};




int num,img_x,img_y;
double dst,dv,helper;
int dst_f_x,dst_f_y;

double min[2] = {9999999999999999,0};
double points_dst[2] = {100000,0};
uint64_t dst_p[2] = {0,0};

double vector[2] = {0,0};

double points0[2] = {0,0};
double points1[2] = {0,0};


double get_dst(uint64_t *crd1, uint64_t *crd2){
    dst_f_x = abs(crd2[0]-crd1[0]);
    dst_f_y = abs(crd2[1]-crd1[1]);
    return sqrt(pow(dst_f_x,2)+pow(dst_f_x,2))/2;
}

void compute(uint8_t **R,uint8_t **G,uint8_t **B,int h,int w,uint8_t **R_,uint8_t **G_,uint8_t **B_,int h_,int w_,uint64_t **points_real,int l_real,uint64_t **points_calc,int l_calc,uint64_t **points_high, int l_high){
    for(int y = 0; y < h;y++){
        for(int x = 0; x < w; x++){
            for(int i = 0; i < l_real; i++){
                dst_p[0] = x;
                dst_p[1] = y;
                dst = get_dst(dst_p,points_real[i]);
                //printf("%i  %i   %i   %i   %lf\n",dst_p[0],dst_p[1],points_real[i][0],points_real[i][1],dst);
                if(dst < min[0]){
                    min[0]=dst;
                    min[1]=i;
                }
                
            }
            
            num = (min[1] / l_real)*l_calc+0.5;
            points_dst[0] = 99999999;
            points_dst[1] = 0;
            points0[0] = 0;
            points0[1] = 0;
            points1[0] = 0;
            points1[1] = 0;
            
            for(int i = 0; i < l_high; i++){
                dst = get_dst(points_high[i],points_calc[num]);
                //printf("%i  %i  %i   %i   %i   %lf\n",i,points_high[i][0],points_high[i][1],points_calc[num][0],points_calc[num][1],dst);
                if(dst<points_dst[0]){
                    //printf("%lf    %lf\n",dst,points_dst[0]);
                    points_dst[1]=points_dst[0];
                    points1[0]=points0[0];
                    points1[1]=points0[1];
                    points_dst[0] = dst; 
                    points0[0] = points_high[i][0];
                    points0[1] = points_high[i][1];
                }
            }
            
            //printf("\n%lf  %lf  %lf   %lf\n",points0[0],points0[1],points1[0],points1[1]);
            
            vector[0]=points1[0]-points0[0];
            vector[1]=points1[1]-points0[1];
            //printf("%lf      %lf\n",vector[0],vector[1]);
            dv = min[0]/sqrt(pow(vector[0],2)+pow(vector[1],2));
            if(y>points_real[(int)(min[1]+0.5)][1]){
                helper = vector[0];
                vector[0] = vector[1]*dv;
                vector[1] = -helper*dv;
            }
            else{
                helper = vector[0];
                vector[0] = -vector[1]*dv;
                vector[1] = helper*dv;
            }
            
            img_x=vector[0]+points_calc[num][0]+0.5;
            img_y=vector[1]+points_calc[num][1]+0.5;
           
            //printf("%i : %i    %i : %i\n",x,y,img_x,img_y);
            
            R[y][x]=R_[img_y][img_x];
            G[y][x]=G_[img_y][img_x];
            B[y][x]=B_[img_y][img_x];
            
            
            
            
        }
        printf("Dokončeno: %i %i %\n",y,h);
    }
}
