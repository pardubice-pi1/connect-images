import numpy as np
import cv2
import txtRead as ReadTXT
import psutil
#import pyproj
import math
import numpy.ctypeslib as ctl
import ctypes
from ctypes import *
from numpy.ctypeslib import ndpointer

TS=ctl.load_library('transform.so', './')
_doublepp = ctl.ndpointer(dtype=np.uintp, ndim=1, flags='C')
TS.compute.argtypes = [_doublepp,_doublepp,_doublepp,ctypes.c_int,ctypes.c_int,_doublepp,_doublepp,_doublepp,ctypes.c_int,ctypes.c_int,_doublepp,ctypes.c_int,_doublepp,ctypes.c_int,_doublepp,ctypes.c_int]

DATA = ReadTXT.getDATA()
#geod = pyproj.Geod(ellps='WGS84')
#print(DATA[1][42][3])
#42  692
#rozsah=[483,492]


rozsah=[42,690]

non=[333,405]




soft = 10 #počet pixelů, které se zjemňují na přechodu na každou stranu
R = 6371*math.pi
pix = 3.067872415#*2.42 #pix = x Km

size = 1608
title = 256

size_diag = math.sqrt(size**2+size**2)/2

posunX = 0
posunY = 0

last_M=[0,0]
max_M=[0,0]
min_M=[0,0]

lines = list()

## UPOZORNĚNÍ PRO PŘEPISOVÁNÍ, KÓD JE PŘEDIMENZOVANÝ!! NĚKTERÉ OPERACE JSOU POSTRADATELNÉ !!! ##

def spoj(pole,zx1,zy1,zx2,zy2,k):
    x1 = zx1
    y1 = zy1
    x2 = zx2
    y2 = zy2
    if(zy2-zy1 < 0 and abs(zx2-zx1) < abs(zy2-zy1)):
        x1 = zx2
        y1 = zy2
        x2 = zx1
        y2 = zy1
    dx=int(((x2-x1)*k)+0.5)
    dy=int(((y2-y1)*k)+0.5)
    
    if (dy == 0):
        for a in range(dx+1):
            pole.append([x1+a,y1])
            
    elif (dx == 0):
        for a in range(dy+1):
            pole.append([x1,y1+a])

    elif (dx>=dy):
        for a in range(dx+1):
            pole.append([x1+a,y1+dy/dx*a+0.5])

    elif (dx<dy):
        for a in range(dy+1):
            pole.append([x1+dx/dy*a+0.5,y1+a])
    return pole

def omez(co,jak):
    if(co>=jak):co=jak-1
    if(co<0):cd=0
    return co

def omezO(co):
    if(co<0):cd=0
    return co

last_A = [0,0]
last_B = [0,0]
last_C = [0,0]

def get_shift(lon1,lat1,lon2,lat2):
    lon, lat = lon1-lon2,lat1-lat2
    lon = (lon/360)*R
    lat = (lat/360)*R
    return lon,lat
    
    
def pixel_shift_compute(real,computed):
    dst_computed = math.sqrt(computed[0]**2 + computed[1]**2)
    dst_real = math.sqrt(real[0]**2 + real[1]**2)
    
    num = dst_real/dst_computed
    
    return [real[0]*num,real[1]*num]
    
    

def get_border_points(start, move,i):
    global last_A
    global last_B
    global last_C
    
    if(start == move):
        A=[int(start[0]),int(start[1]+(size-move[1]))]
        B=[int(start[0]+(size-move[0])),int(start[1]+(size-move[1]))]
        C=[int(start[0]+(size-move[0])),int(start[1])]
    else: 
        A = [int((last_A[0]+move[0])-i*0.0),int((last_A[1]+move[1]))]
        B = [int((last_B[0]+move[0])-i*0.0),int((last_B[1]+move[1]))]
        C = [int((last_C[0]+move[0])-i*0.0),int((last_C[1]+move[1]))]
        
    last_A=list(A)
    last_B=list(B)
    last_C=list(C)
    return [[A,B],[C,B]]


def soft_border(img_,lin_hor,lin_ver):
    img_ = cv2.cvtColor(img_,cv2.COLOR_BGR2YCR_CB)
    #horizontal
    y0 = omez(lin_hor[0][1],len(img_))
    for x in range(omez(lin_hor[0][0],len(img_[0])),omez(lin_hor[1][0],len(img_[0]))):
        side1=0
        for y in range(omez(y0-soft,len(img_)),y0):side1+=img_[y][x][0]
        side2=0
        for y in range(y0,omez(y0+soft,len(img_))):side2+=img_[y][x][0]
        side1/=soft
        side2/=soft
        avg = (side1+side2)/2
        side1 = (side1-avg)/soft
        side2 = (side2-avg)/soft
        img_[y0][x][0] = avg
        for i in range(1,soft):
            if(y0-i>=0):img_[y0-i][x][0] = avg+(side1*i)
            if(y0+i<len(img_)):img_[y0+i][x][0] = avg+(side2*i)
            
        
        
        
        
    #vertical
    x0 = omez(lin_ver[0][0],len(img_[0]))
    for y in range(omez(lin_ver[0][1],len(img_)),omez(lin_ver[1][1],len(img_))):
        side1=0
        for x in range(omez(x0-soft,len(img_[0])),x0):side1+=img_[y][x][0]
        side2=0
        for x in range(x0,omez(x0+soft,len(img_[0]))):side2+=img_[y][x][0]
        side1/=soft
        side2/=soft
        avg = (side1+side2)/2
        side1 = (side1-avg)/soft
        side2 = (side2-avg)/soft
        img_[y][x0][0] = avg
        for i in range(1,soft):
            if(x0-i>=0):img_[y][x0-i][0] = avg+(side1*i)
            if(x0+i<len(img_[0])):img_[y][x0+i][0] = avg+(side2*i)
        
    img_ = cv2.cvtColor(img_,cv2.COLOR_YCR_CB2BGR)
    return img_


def orez(img,x,y,r):
    rectX = (x - r)
    rectY = (y - r)
    return img[rectY:(y+2*r),rectX:(x+2*r)]
    


def get_M(img2,img1):
    #M = np.zeros((3,3))
    
    gray1 = cv2.cvtColor(img1,cv2.COLOR_BGR2GRAY)
    gray2 = cv2.cvtColor(img2,cv2.COLOR_BGR2GRAY)

    sift = cv2.xfeatures2d.SIFT_create()
    #sift = cv2.ORB_create()

    kp1, des1 = sift.detectAndCompute(gray1,None)
    kp2, des2 = sift.detectAndCompute(gray2,None)
    
    error = 0
    
    if des1 is None:error = 1
    elif des2 is None:error = 1
    elif(len(des1)<=3 or len(des2)<=3):error = 1
    
    if(error == 0):

        #cv2.drawKeypoints(img1,kp1,img1)
        #cv2.drawKeypoints(img2,kp2,img2)# flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

        """
        print("pocet bodu: ",len(kp1),"pocet des:",len(des2))
        print(kp1[0])
        print(des2[0])
        """

        """
        bf = cv2.BFMatcher(cv2.NORM_HAMMING2)
        matches = bf.match(des1,des2)
        matches = sorted(matches, key = lambda x:x.distance)
        """
        index_params = dict(algorithm = 1, trees = 5)
        search_params = dict(checks = 50)

        flann = cv2.FlannBasedMatcher(index_params, search_params)

        matches = flann.knnMatch(des1,des2,k=2)

        #img3 = cv2.drawMatches(img1,kp1,img2,kp2,matches[:10],None,flags=cv2.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)

        """
        print("pocet: ",len(matches))
        print(matches[i].imgIdx)

        print(matches[i].queryIdx)
        print(matches[i].trainIdx)
        """


        good = []

        for m,n in matches:
            if m.distance < 0.7*n.distance:
                good.append(m)
                

        if(len(good) > 10):
            src_pts = np.float32([kp1[m.queryIdx].pt for m in good]).reshape(-1,1,2)
            dst_pts = np.float32([kp2[m.trainIdx].pt for m in good]).reshape(-1,1,2)
            
            M, mask = cv2.findHomography(src_pts,dst_pts,cv2.RANSAC,5.0)
            """
            Mat=np.zeros((3,3))
            Mat[0][2]=M[0][2]
            Mat[1][2]=M[1][2]
            Mat[0,0]=1
            Mat[1,1]=1
            Mat[2,2]=1
            """
            return M

#print("posun x: ",M[0][2])
#print("posun y: ",M[1][2])
lines=list()
def Assemble_compute(M,img1_,img2_,ind):
    Mx=M[0][2]+last_M[0]
    My=M[1][2]+last_M[1]
    move = [M[0][2],M[1][2]]
    
    M[0][2]=0
    M[1][2]=0

    start1=[0,0]
    start2=[0,0]

    y,x,z = img1_.shape
    reload = img1_
    
    if(Mx>max_M[0] or Mx<min_M[0]):img1_ = np.zeros((y,x+int(abs(Mx-last_M[0]+posunX)),3),np.uint8)
    if(My>max_M[1] or My<min_M[1]):img1_ = np.zeros((y+int(abs(My-last_M[1])+posunY),x,3),np.uint8)
    if((My>max_M[1] or My<min_M[1]) and (Mx>max_M[0] or Mx<min_M[0])):  img1_ = np.zeros((y+int(abs(My-last_M[1])+posunY),x+int(abs(Mx-last_M[0])+posunX),3),np.uint8)
    #img1_ = np.zeros((y+int(abs(My)),x+int(abs(Mx)),3),np.uint8)

    if(Mx>=0):
        start1[0]=0
        start2[0]=Mx
    else:
        start1[0]=Mx
        start2[0]=0
        
    if(My>=0):
        start1[1]=0
        start2[1]=My
    else:
        start1[1]=My
        start2[1]=0
        
    #img2_ = cv2.warpPerspective(img2_,M,(size,size))

    y,x,z = reload.shape
    a,b,c = img1_.shape
    Intstart1 = [int(start1[0]),int(start1[1])]
    Intstart2 = [int(start2[0]),int(start2[1])]
    img1_[Intstart2[1]:Intstart2[1]+size,Intstart2[0]:Intstart2[0]+size]=img2_[0:a-Intstart2[1],0:b-Intstart2[0]]
    img1_[Intstart1[1]:Intstart1[1]+y,Intstart1[0]:Intstart1[0]+x]=reload#[0:y-1,0:x-1]
    
    last_M[0]=Mx
    last_M[1]=My
    min_M[0]=max(max_M[0],Mx)
    min_M[1]=max(max_M[1],My)
    max_M[0]=max(max_M[0],Mx)
    max_M[1]=max(max_M[1],My)
    #print(start2,"      ",move)
    
    #lines = get_border_points([start2[0],start2[1]],move,ind)
    #img1_ = soft_border(img1_,lines[0],lines[1])
    M[0][2]=move[0]
    M[1][2]=move[1]
    
    return img1_

def get_M_DATA(id1,id2):
    x1 = DATA[1][id1][1]
    y1 = DATA[1][id1][2]
    x2 = DATA[1][id2][1]
    y2 = DATA[1][id2][2]
    
    Mat=np.zeros((3,3))
    Mat[0][2]=x1-x2
    Mat[1][2]=y1-y2
    Mat[0,0]=1
    Mat[1,1]=1
    Mat[2,2]=1
    
    return Mat

def transform(num,len_ab):
    ret = (num / len_ab[0])*len_ab[1]
    return int(ret)
    
def translate(array,shift):
    for i in range(len(array)):
        array[i][0]+=shift[0]
        array[i][1]+=shift[1]
    return array

def get_dst(crd1,crd2):
    dst_x = abs(crd2[0]-crd1[0])
    dst_y = abs(crd2[1]-crd1[1])
    dst = math.sqrt(dst_x**2+dst_y**2)/2
    return dst



def draw_line(img,points_calc,points_real):
    vzd_calc = 0
    vzd_real = 0

    for i in range(1,len(points_calc)):
        #print(points_calc[i],points_calc[i-1])
        points_calc[i][0] = int(points_calc[i][0] + points_calc[i-1][0]+0.5)
        points_calc[i][1] = int(points_calc[i][1] + points_calc[i-1][1]+0.5)
        points_real[i][0] = int(points_real[i][0] + points_real[i-1][0]+0.5)
        points_real[i][1] = int(points_real[i][1] + points_real[i-1][1]+0.5)
        
        
    for i in range(1,len(points_calc)):
        vzd_calc = vzd_calc + math.sqrt((abs(points_calc[i][0]-points_calc[i-1][0]))**2+(abs(points_calc[i][1]-points_calc[i][1]))**2)
        vzd_real = vzd_real + math.sqrt((abs(points_real[i][0]-points_real[i-1][0]))**2+(abs(points_real[i][1]-points_real[i][1]))**2)
    k = vzd_calc/vzd_real
    #print("vzdálenosti čar: ",vzd_calc,vzd_real," Sjednocovací koeficient: ",k)

    #last_points = list(points_real)
    for i in range(1,len(points_real)):
        points_real[i][0] = int((points_real[i][0]-points_real[0][0])*k)+points_real[i][0]
        points_real[i][1] = int((points_real[i][1]-points_real[0][1])*k)+points_real[i][1]
        
    for i in range(1,len(points_calc)): #kresba čar
        cv2.line(img,(int(points_calc[i-1][0]+0.5),int(points_calc[i-1][1]+0.5)),(int(points_calc[i][0]+0.5),int(points_calc[i][1]+0.5)),(255,0,0),10)
        cv2.circle(img,(int(points_calc[i][0]+0.5),int(points_calc[i][1]+0.5)),18,(255,0,0),-1)
    for i in range(1,len(points_real)): #kresba čar
        cv2.line(img,(int(points_real[i-1][0]+0.5),int(points_real[i-1][1]+0.5)),(int(points_real[i][0]+0.5),int(points_real[i][1]+0.5)),(0,0,255),10)
        cv2.circle(img,(int(points_real[i][0]+0.5),int(points_real[i][1]+0.5)),18,(0,0,255),-1)
        
        


############################################################################################

img1 = cv2.imread(DATA[1][rozsah[0]][3])

#size = size / 2 ##########################################################################
#img1 = cv2.resize(img1,(int(len(img1[0])/2),int(len(img1)/2)))###########################

size_y=int(len(img1)/2)
size_x=int(len(img1[0])/2)
img1 = orez(img1,size_x-250,size_y-385,536)
img = img1

last_status = -1
last_memory = 0

split = list()
points_calc = list()
points_real = list()
points_calc.append([size/2,size/2])
points_real.append([size/2,size/2])

ram_start = int(psutil.virtual_memory()[3]/1000000)

img_zero = np.zeros((size,size,3),np.uint8)

non_e = 0

for i in range(rozsah[0]+1,rozsah[1]+1):
    img2 = cv2.imread(DATA[1][i][3])
    
    #img2 = cv2.resize(img2,(int(len(img2[0])/2),int(len(img2)/2))) #########################
    
    img2 = orez(img2,size_x-250,size_y-385,536)
    
    #M = get_M_DATA(i-1,i)
    M=get_M(img1,img2)
    #memory.append(psutil.virtual_memory()[3]/1000000000)
    if M is None:
        print("Nebyla nalezena souvislost mezi snímky: ",DATA[1][i-1][3]," a ",DATA[1][i][3])
        M=last_Mat
        
    
    #print("posun o: ",M[0][2],M[1][2])
    
    if(i>non[0] and i<non[1]):
        M=last_Mat
        img=Assemble_compute(M,img,img_zero,i)
        print("snímek",i,"je označen jako nekvalitní, je tedy odstraněn")
        non_e = 1
    else:
        img=Assemble_compute(M,img,img2,i)
        non_e = 0
    
    last_Mat = M
    
    status = ((i-rozsah[0]+1)/(rozsah[1]+1-rozsah[0]))*100
    status = "{:.2f}".format(status)
    print("Dokončeno:",status,"%   snímek číslo:",i,end="")
    print("    Přibilo:",int(psutil.virtual_memory()[3]/1000000-last_memory),"MB     snímky zabírají: ", int(psutil.virtual_memory()[3]/1000000)-ram_start,end = " MB ")
    
    last_memory = psutil.virtual_memory()[3]/1000000
    
    
    
    
    img1=img2
    shift = get_shift(DATA[1][i][1],DATA[1][i][2],DATA[1][i-1][1],DATA[1][i-1][2])
    points_calc.append([last_Mat[0][2],last_Mat[1][2]])
    points_real.append([shift[0]*pix,-shift[1]*pix])
    #shift = pixel_shift_compute([shift[0],-shift[1]],[last_Mat[0][2],last_Mat[1][2]])
    #points_real.append([shift[0],-shift[1]])
    print("    posuny: ",points_calc[len(points_calc)-1],"     ",points_real[len(points_real)-1])
    #print("posuny: ",last_Mat[0][2],last_Mat[1][2],"",shift[0],shift[1],end="       koeficient: ")
    #print(last_Mat[0][2]*shift[0],last_Mat[1][2]*shift[0])
    
    if(last_memory > 10000): #3000
        name = "spoj-"+str(i)+".jpg"
        max_M=[0,0]
        min_M=[0,0]
        #draw_line(img,points_calc,points_real)
        points_calc = list()
        points_real = list()
        points_calc.append([size/2,size/2])
        points_real.append([size/2,size/2])
        cv2.imwrite(name,img)
        del(img)
        img = img1
        if(non_e == 1):
            img=img_zero
            non_e = 0
        else:
            last_M=[0,0]
        print("Z důvodu přetečení paměti, byl obraz rozdělen aby se předešlo vypnutí programu. Obraz uložen jako: ",name)
        split.append([name,M])
        
#draw_line(img,points_calc,points_real)

split.append(["spoj-koncovka.jpg",M])
cv2.imwrite("spoj-koncovka.jpg",img)

print("Výpočet dokončen, provádím finální spojení...")

del(img1)
del(img2)
#del(img)

rozX=0
rozY=0
x = list()
y = list()
x.append(0)
y.append(0)

"""

for i in range(len(split)):
    img = cv2.imread(split[i][0])
    #print(split[i][0])
    rozX += len(img[0])-size
    rozY += len(img)-size
    y.append(y[i-1]+len(img))
    x.append(x[i-1]+len(img[0]))
    
    
for z in range(5):
    n = 2**z
    resize = ((title*n)/rozX)
    d_y = ((title*n)-resize*rozY)/2
    print(n**2,"  celkove rozmery: ",rozX,":",rozY,"  rozmery po resizu:",int(resize*rozX+0.5),int(resize*rozY+0.5),"na osu Y je potřeba dodělat: ",int(d_y+0.5),int(d_y+0.5)," = ",int(d_y)+int(d_y+0.5),"(",d_y,") pix")
    img = np.zeros((int(title*n),int(title*n),3),np.uint8)
    for i in range(len(split)):
        dX = (x[i+1]-x[i]+size)
        dY = (y[i+1]-y[i]+size)
        #print("Výpočet diferencí: ",i,"  ",int(dX*resize),int(dY*resize),end="         ")
        img_split = cv2.imread(split[i][0])
        img_split = cv2.resize(img_split,(int(dX*resize+0.5),int(dY*resize+0.5)),interpolation = cv2.INTER_AREA)
        #print("výpočet:   ",int(d_y+0.5+(y[i]*resize)),int((y[i]*resize)+d_y+(dY*resize)+0.5),"     ",int((x[i]*resize)+0.5),int((x[i]*resize)+(dX*resize)+0.5))
        
        copX = [((x[i]*resize)+69),(x[i]*resize)+(dX*resize)+69]
        copY = [int(d_y+0.5+(y[i]*resize)),int((y[i]*resize)+d_y+(dY*resize)+0.5)]
        
        print([copX[0],copX[1],"      ",copY[0],copY[1]],"            ", dY*resize+0.5,dX*resize+0.5,"po resizu má ve skutečnosti: ", len(img_split[0]),len(img_split))
        
        img[int(copY[0]+0.5):int(copY[1]+0.5)][int(copX[0]+0.5):int(copX[1]+0.5)] = img_split[0:int(dY*resize+0.5)][0:int(dX*resize+0.5)]
        print("")
    #for i in range(1,n):
    cv2.imwrite("res/"+z+"final.jpg",img)
    del(img)


"""

#del(img)
#print("Při výpočtu bylo nutné obraz rozdělit na",len(split),"dílů. Probíhá příprava na Transformaci...")


"""

points_all_calc = list()
points_all_real = list()

edges = [len(img[0]),0,len(img),0]

for i in range(1,len(points_calc)):
    points_all_calc = spoj(points_all_calc,points_calc[i-1][0],points_calc[i-1][1],points_calc[i][0],points_calc[i][1],1)
for i in range(1,len(points_real)):
    points_all_real = spoj(points_all_real,points_real[i-1][0],points_real[i-1][1],points_real[i][0],points_real[i][1],1)
    
for i in range(len(points_real)):
    if(points_real[i][0]<edges[0]):edges[0]=int(points_real[i][0])
    if(points_real[i][0]>edges[1]):edges[1]=int(points_real[i][0])
    if(points_real[i][1]<edges[2]):edges[2]=int(points_real[i][1])
    if(points_real[i][1]>edges[3]):edges[3]=int(points_real[i][1])
    
    
#all_size = [int((edges[1]+size_diag)-(edges[0]-size_diag)+0.5),int((edges[3]+size_diag)-(edges[2]-size_diag)+0.5)]
all_size = [int((edges[1]-edges[0])+0.5+size_diag),int((edges[3]-edges[2])+0.5+size_diag)]

all_connect = [[size_diag,size_diag],[points_real[len(points_real)-1][0]-size_diag,points_real[len(points_real)-1][1]-size_diag]]

all_len = [len(points_all_real),len(points_all_calc)]
#print("vzdálenost celková: ",len(points_all_calc),len(points_all_real)," Sjednocovací koeficient: ",k2)


for i in range(0,len(points_all_calc),30):
    cv2.circle(img,(int(points_all_calc[i][0]+0.5),int(points_all_calc[i][1]+0.5)),5,(255,0,0),-1)
    cv2.circle(img,(int(points_all_real[i][0]+0.5),int(points_all_real[i][1]+0.5)),5,(0,0,255),-1)

print(edges)
print("Provádím finální tranformaci obrazu...   all_size:",all_size)


img_transform = np.zeros(((all_size[1]),(all_size[0]),3),np.uint8)
points_all_real = translate(points_all_real,[size/2-all_connect[0][0],size/2-all_connect[0][1]])



R = img_transform[:,:,2]
G = img_transform[:,:,1]
B = img_transform[:,:,0]
R_ = img[:,:,2]
G_ = img[:,:,1]
B_ = img[:,:,0]

Rc = (R.__array_interface__['data'][0] + np.arange(R.shape[0])*R.strides[0]).astype(np.uintp)
Gc = (G.__array_interface__['data'][0] + np.arange(G.shape[0])*G.strides[0]).astype(np.uintp)
Bc = (B.__array_interface__['data'][0] + np.arange(B.shape[0])*B.strides[0]).astype(np.uintp)
R_c = (R_.__array_interface__['data'][0] + np.arange(R_.shape[0])*R_.strides[0]).astype(np.uintp)
G_c = (G_.__array_interface__['data'][0] + np.arange(G_.shape[0])*G_.strides[0]).astype(np.uintp)
B_c = (B_.__array_interface__['data'][0] + np.arange(B_.shape[0])*B_.strides[0]).astype(np.uintp)

points_all_real_ = np.array(points_all_real,dtype = np.uint64)
points_all_calc_ = np.array(points_all_calc,dtype = np.uint64)
points_calc_ = np.array(points_calc,dtype = np.uint64)

#print(len(points_all_real_),len(points_all_real_[0]),"   ",len(points_all_real),len(points_all_real[0]),)

points_all_real_c = (points_all_real_.__array_interface__['data'][0] + np.arange(points_all_real_.shape[0])*points_all_real_.strides[0]).astype(np.uint64)
points_all_calc_c = (points_all_calc_.__array_interface__['data'][0] + np.arange(points_all_calc_.shape[0])*points_all_calc_.strides[0]).astype(np.uint64)
points_calc_c = (points_calc_.__array_interface__['data'][0] + np.arange(points_calc_.shape[0])*points_calc_.strides[0]).astype(np.uint64)


#!!převést do C jazyka!!
TS.compute(Rc,Gc,Bc,len(img_transform),len(img_transform[0]),R_c,G_c,B_c,len(img),len(img[0]),points_all_real_c,len(points_all_real),points_all_calc_c,len(points_all_calc),points_calc_c,len(points_calc))

img_transform[:,:,2] = R
img_transform[:,:,1] = G
img_transform[:,:,0] = B


for y in range(0,len(img_transform),1):
    for x in range(0,len(img_transform[0]),1):
        min = [len(img_transform[0]),0]
        for i in range(len(points_all_real)):
            if(get_dst([x,y],points_all_real[i])<min[0]):
                min[0]=get_dst([x,y],points_all_real[i])
                min[1]=i
        num = transform(min[1],[len(points_all_real),len(points_all_calc)])
        points_dst = [100000,0]
        points = [[0,0],[0,0]]
        for i in range(len(points_calc)):
            dst = get_dst(points_calc[i],points_all_calc[num])
            if(dst<points_dst[0]):
                points_dst[1]=points_dst[0]
                points[1]=points[0]
                points_dst[0]=dst
                points[0]=points_calc[i]
        vector = [(points[1][0]-points[0][0]),(points[1][1]-points[0][1])]
        dv = min[0]/math.sqrt(vector[0]**2+vector[1]**2)
        if(y>points_all_real[min[1]][1]):vector[0],vector[1] = vector[1]*dv,-vector[0]*dv #potřeba zautomatizovat převrázení vektoru
        else:vector[0],vector[1] = -vector[1]*dv,vector[0]*dv
        #print(int(vector[0]+points_all_calc[num][0]+0.5),int(vector[1]+points_all_calc[num][1]+0.5),"     ",math.sqrt(vector[0]**2+vector[1]**2),dst)
        img_x = int(vector[0]+points_all_calc[num][0]+0.5)
        img_y = int(vector[1]+points_all_calc[num][1]+0.5)
        
        if(img_y < 0 or img_x < 0 or img_y >= len(img_transform) or img_x >= len(img_transform[0])):
            img_transform[y,x]=[0,0,0]
        else:
            img_transform[y,x]=img[img_y,img_x]
        #cv2.line(img_transform,(x,y),(int(points_all_real[min[1]][0]),int(points_all_real[min[1]][1])),(255,0,0),4)
        #cv2.line(img,(int(vector[0]+points_all_calc[num][0]+0.5),int(vector[1]+points_all_calc[num][1]+0.5)),(int(points_all_calc[num][0]),int(points_all_calc[num][1])),(255,0,0),4)
        #cv2.circle(img_transform,(x,y),7,(0,x+y,255),-1)
        #cv2.circle(img,(int(vector[0]+points_all_calc[num][0]+0.5),int(vector[1]+points_all_calc[num][1]+0.5)),7,(0,x+y,255),-1)
        
        
        del(points)
        del(points_dst)
        #print(points_all_calc[num])
    status = ((y/len(img_transform))*100)
    status = "{:.2f}".format(status)
    print("Dokončeno: ",status,"%")
    


for i in range(0,len(points_all_calc)):
    cv2.circle(img,(int(points_all_calc[i][0]+0.5),int(points_all_calc[i][1]+0.5)),3,(0,0,255),-1)
for i in range(0,len(points_all_real)):
    cv2.circle(img_transform,(int(points_all_real[i][0]+0.5),int(points_all_real[i][1]+0.5)),3,(0,0,255),-1)








print("Výpočet byl Dokončen, provádím tranformaci podle reálných GPS pozic...")


for i in range(1,len(points_calc)):
    #print(points_calc[i],points_calc[i-1])
    points_calc[i][0] = int(points_calc[i][0] + points_calc[i-1][0]+0.5)
    points_calc[i][1] = int(points_calc[i][1] + points_calc[i-1][1]+0.5)
    points_real[i][0] = int(points_real[i][0] + points_real[i-1][0]+0.5)
    points_real[i][1] = int(points_real[i][1] + points_real[i-1][1]+0.5)

points_calc_edge = list()
points_real_edge = list()
points_calc_edge.append([[size,size],[size,0]])
points_real_edge.append([[size,size],[size,0]])


for i in range(1,len(points_calc)):
    #print([(points_calc[i][0]+size/2),(points_calc[i][1]+size/2)],[(points_calc[i][0]-size/2),(points_calc[i][1]+size/2)])
    points_calc_edge.append([[(points_calc[i][0]+size/2),(points_calc[i][1]+size/2)],[(points_calc[i][0]+size/2),(points_calc[i][1]-size/2)]])
    points_real_edge.append([[(points_real[i][0]+size/2),(points_real[i][1]+size/2)],[(points_real[i][0]+size/2),(points_real[i][1]-size/2)]])

#print(points_calc)
#print("-------------------")
#print(points_real)

cv2.circle(img,(int(points_calc[0][0]+0.5),int(points_calc[0][1]+0.5)),10,(255,0,0),-1)
cv2.circle(img,(int(points_real[0][0]+0.5),int(points_real[0][1]+0.5)),10,(0,0,255),-1)

for i in range(1,len(points_calc)): #kresba čar
    cv2.line(img,(int(points_calc[i-1][0]+0.5),int(points_calc[i-1][1]+0.5)),(int(points_calc[i][0]+0.5),int(points_calc[i][1]+0.5)),(255,0,0),6)
    cv2.line(img,(int(points_real[i-1][0]+0.5),int(points_real[i-1][1]+0.5)),(int(points_real[i][0]+0.5),int(points_real[i][1]+0.5)),(0,0,255),6)
    cv2.circle(img,(int(points_calc[i][0]+0.5),int(points_calc[i][1]+0.5)),10,(255,0,0),-1)
    cv2.circle(img,(int(points_real[i][0]+0.5),int(points_real[i][1]+0.5)),10,(0,0,255),-1)

cv2.imwrite("spoj.jpg",img)
cv2.imwrite("spoj-tranform.jpg",img_transform)


#img = cv2.cvtColor(img,cv2.COLOR_BGR2YCR_CB)

"""

while True:
    #cv2.imshow('1',cv2.resize(img1,(int(len(img1[0])/2),int(len(img1)/2)),interpolation = cv2.INTER_AREA))
    #cv2.imshow('2',cv2.resize(img2,(int(len(img2[0])/2),int(len(img2)/2)),interpolation = cv2.INTER_AREA))
    
    
    
    rsize = 10
    cv2.imshow('RET',cv2.resize(img,(int(len(img[0])/rsize),int(len(img)/rsize))))
    #cv2.imshow('transform',cv2.resize(img_transform,(int(len(img_transform[0])/rsize),int(len(img_transform)/rsize))))
    #cv2.imshow('dst',dst)
    
    if(cv2.waitKey(1)&0xFF == ord('q')):
        break
    
    if(cv2.waitKey(1)&0xFF == ord('s')):
        cv2.imwrite("spoj.jpg",img)

#"""
