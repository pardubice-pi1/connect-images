import numpy as np
import math
#import utm

file = open('zz_pardubicepi_output.log','r')
lines = file.read().splitlines()
file.close()

north = 0
longitude = 0
latitude = 0
index = 0
ia=0
ib=0
R = 6371*math.pi
#pixel = 3.352941176470588
pix = 3.067872415 #pix = x Km

array = list()
for i in range(1000):
    array.append([0,0,0,"-"])
DATA = [0,array]
"""
def latlon2relativeXY(lat,lon):
    #lon = lon/180*math.pi
    #lat = lat/180*math.pi
    x = (lon + 180) / 360
    y = (1 - log(tan(radians(lat)) + sec(radians(lat))) / pi) / 2
    return [x,y]
"""

def sec(x):
  return(1/math.cos(x))

def toNum(a):
    ret=0
    if(a=='0'):ret=0
    if(a=='1'):ret=1
    if(a=='2'):ret=2
    if(a=='3'):ret=3
    if(a=='4'):ret=4
    if(a=='5'):ret=5
    if(a=='6'):ret=6
    if(a=='7'):ret=7
    if(a=='8'):ret=8
    if(a=='9'):ret=9
    return ret
    
def toInt(l):
    ret=0
    for i in range(len(l)):
        ret=ret*10+int(toNum(l[i]))
    return ret
"""
def toXY(long,lat):
    x,y = utm.from_latlon(long,lat)
    return [x,y]
"""

MAX_lat = 20
MAX_lat_id = 0
norths = list()

def getDATA():
    global MAX_lat
    global MAX_lat_id
    global norths
    
    for i in range(2,len(lines)-1):
        line = lines[i].split()
        n=line[len(line)-1]
        main = 0
        
        for x in range(len(line[3])):
            if(line[3][x]==":"):
                main = toInt(line[3][x:len(line[3])-1])
                
        
        if(main==49):
            north=n[0:len(n)-1]
        
        
        if(main==66):
            for x in range(len(n)):
                if(n[x]=="°"):
                    ia=x
                if(n[x]=="'"):
                    ib=x
                    break
            a=toInt(list(n[0:ia]))
            b=toInt(n[ia+1:ib])
            c=toInt(n[ib+1:len(n)-2])
            c=(c/60)
            b+=c
            b=(b/60)
            longitude=a+b
        
        
        if(main==79):
            for x in range(len(n)):
                if(n[x]=="°"):
                    ia=x
                if(n[x]=="'"):
                    ib=x
                    break
            a=toInt(list(n[0:ia]))
            b=toInt(n[ia+1:ib])
            c=toInt(n[ib+1:len(n)-2])
            c=(c/60)
            b+=c
            b=(b/60)
            latitude=a+b
            
            
        if(main==144):
            index=toInt(line[5][40:44])
            #name = line[5][13:48]
            name = line[5][13:27]+"_"+line[5][28:48]
            #posX = R * math.cos(latitude) * math.cos(longitude)
            #posY = R * math.cos(latitude) * math.sin(longitude)
            posX = 0
            posY = 0
            #print("Position XY: ",posX,posY)
            DATA[0]=max(DATA[0],index)
            DATA[1][index]=[north,longitude,latitude,name,posX,posY]
            if(latitude > MAX_lat and index != 369):
                MAX_lat = latitude
                MAX_lat_id = index
            
            #DATA[1][index]=[north,posX,posY,name]
            north=0
            longitude=0
            latitude=0
    #print("Maximální zeměpisná šířka:",MAX_lat," s číslem ",MAX_lat_id)
    #print(norths)
    return DATA
    #print(index)
"""
d=getDATA()
for n in range(1,d[0]):
    print(n,"               ",d[1][n])
    
#"""
   
